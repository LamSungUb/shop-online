<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::create([
            'name'      => 'Đào Mạnh Lâm',
            'email'     => 'lamsungub@gmail.com',
            'password'  =>  bcrypt('123456'),
            'confirmed' =>  1,
        ]);
        factory(Customer::class, 49)->create();
    }
}
