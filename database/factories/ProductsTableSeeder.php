<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Product::class, function (Faker $faker) {
    return [
        'product_name' 			=> $faker->word,
        'slug' 			        => $faker->slug,
        'price'			        => $faker->randomNumber(5),
        'image'			        => $faker->image('public\Images\product_image',395, 395, false),
        'discount'              => $faker->randomNumber(2),
        'category_id'           => $faker->numberBetween(1,3),
        'status'                => 1,
        'quantity'              => $faker->randomNumber(3),
        'description'           => $faker->word

    ];
});
