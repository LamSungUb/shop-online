<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('confirmed')->default(0)->comment('Trạng thái của tài khoản : 0 - chưa xác nhận, 1 - xác nhận');
            $table->string('confirmation_code')->nullable()->comment('Mã code tạo ra ngẫu nhiên khi tạo tài khoản');
            $table->string('code')->nullable()->index()->comment('Mã code để lấy lại mật khẩu');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
