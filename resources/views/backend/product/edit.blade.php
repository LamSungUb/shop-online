@extends('backend.layout.master')
@section('title','Cập Nhập Sản Phẩm')
@section('content')
    <section class="container">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Sửa sản phẩm</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Sửa ảnh thumbnail</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <section class="container">
                    <form method="post" action="{{ route('admin.product.update',$product->id) }}" enctype="multipart/form-data" >
                        @csrf
                        @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                {{Session::get('success')}}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only" style="font-size: 35px">Close</span>
                                </button>
                            </div>
                        @endif
                        <div class="form-group row">
                            <label class="col-3 col-form-label" for="product_name">ID sản phẩm</label>
                            <div class="col-9">
                                <b>{{ $product->id }}</b>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3 col-form-label" for="product_name">Tên sản phẩm</label>
                            <div class="col-9">
                                <input id="product_name"  name="product_name" type="text" class="form-control" value="{{ $product->product_name }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image" class="col-3 col-form-label" >Ảnh</label>
                            <div class="col-9">
                                <input type="file" accept="image/*"  id="image" name="image" onchange="loadFile(event)" style="display: none;" >
                                <img height="260px" width="260px" id="output" src="{{asset('Images') }}/product_image/{{ $product->image }}">
                                <label class="form-label" for="image" style="cursor: pointer;">
                                    <p class="btn btn-outline-success"><i class="fa fa-upload"></i></p>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="price" class="col-3 col-form-label">Giá</label>
                            <div class="col-9">
                                <input id="price" value="{{ $product->price }}" name="price" type="number" min="0" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="discount" class="col-3 col-form-label">Khuyến mãi (%)</label>
                            <div class="col-9">
                                <input id="discount" value="{{ $product->discount }}" name="discount" min="0" max="100" type="number" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="category_id" class="col-3 col-form-label">Danh mục sản phẩm</label>
                            <div class="col-9">
                                <select id="category_id" name="category_id" class="custom-select">
                                    @foreach($categories as $category)
                                        @if($product->category_id == $category->id)
                                            <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                        @endif
                                    @endforeach
                                    @foreach($cateNotId as $cate)
                                        <option value="{{ $cate->id }}">{{ $cate->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="quantity" class="col-3 col-form-label">Số lượng</label>
                            <div class="col-9">
                                <input id="quantity" value="{{ $product->quantity }}" name="quantity" type="number" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-3 col-form-label">Mô tả</label>
                            <div class="col-9">
                                <textarea id="description" name="description" cols="80" rows="10">{!! $product->description !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="offset-3 col-9">
                                <button type="submit" class="btn btn-primary">Cập nhập sản phẩm</button>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
               <section class="container">
                   <div class="row">
                       <div class="col-6">
                           <table class="table table-hover">
                               <thead>
                               <tr>
                                   <th scope="col">#</th>
                                   <th scope="col">Ảnh</th>
                                   <th scope="col">ID Sản phẩm</th>
                                   <th scope="col">Hành Động</th>
                               </tr>
                               </thead>
                               <tbody>
                               @foreach($thumb as $key => $value)
                                   <tr>
                                       <th scope="row">{{ $key+1 }}</th>
                                       <td>
                                           <img height="40px" width="40px" src="{{ asset('Images') }}/product_thumbnail/{{ $value->thumbnail_name }}">
                                       </td>
                                       <td>{{ $value->product_id }}</td>
                                       <td>
                                           <a href="{{ route('admin.thumb.edit',$value->id) }}" > <i class="fa fa-tools"></i></a> -
                                           <a href="{{ route('admin.thumb.delete',$value->id) }}"><i class="fa fa-trash-alt"></i></a>
                                       </td>
                                   </tr>
                               @endforeach
                               </tbody>
                           </table>
                       </div>
                       <div class="col-6">
                           <a href="{{ route('admin.thumb.create',$product->id) }}" class="btn btn-primary" >Thêm ảnh thumbnail</a>
                       </div>
                   </div>
               </section>
            </div>
        </div>
    </section>
@endsection

@section('script')
    {{-- js upload 1 ảnh --}}
    <script>
        var loadFile = function(event)
        {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
    {{-- ckeditor --}}
    <script src="{{asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{asset('ckfinder/ckfinder.js') }}"></script>

    <script>
        CKEDITOR.replace( 'description' );
    </script>

    <script>
        setInterval(function(){
            $('.alert-success').hide(2000);
        }, 3000);
    </script>
@endsection



