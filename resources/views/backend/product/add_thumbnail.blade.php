@extends('backend.layout.master')
@section('title','Thêm Thumbnail Sản Phẩm')
@section('content')
    <section class="container">
        @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{Session::get('success')}}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only" style="font-size: 35px">Close</span>
                </button>
            </div>
        @endif
        <div class="row">
            <div class="tab-content" id="v-pills-tabContent">
                <form method="post" action="{{ route('admin.thumb.store',$productId) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="container">
                        <div class="form-group row">
                            <label for="image" class="col-3 col-form-label" >Thêm ảnh thumbnail</label>
                            <div class="col-9">
                                <input type="file" accept="image/*"  id="image" name="image" onchange="loadFile(event)" style="display: none;" >
                                <img height="260px" width="260px" id="output" >
                                <label class="form-label" for="image" style="cursor: pointer;">
                                    <p class="btn btn-outline-primary">Chọn ảnh</p>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3"></div>
                            <div class="col-9">
                                <button class="btn btn-outline-primary" type="submit">Thêm</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    {{-- js upload 1 ảnh --}}
    <script>
        var loadFile = function(event)
        {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>

    <script>
        setInterval(function(){
            $('.alert-success').hide(2000);
        }, 3000);
    </script>
@endsection



