@extends('backend.layout.master')
@section('title','Thêm Sản Phẩm')
@section('css')
    <style type="text/css">
        .thumb-image{
            float:left;
            width:100px;
            position:relative;
            padding-right: 10px;
            padding-top: 10px;
        }
    </style>
@section('content')
    <section class="container">
        <form method="post" action="{{ route('admin.product.store') }}" enctype="multipart/form-data" >
            @csrf

            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only" style="font-size: 35px">Close</span>
                    </button>
                </div>
            @endif

            <div class="form-group row">
                <label class="col-3 col-form-label" for="product_name">Tên sản phẩm</label>
                <div class="col-9">
                    <input id="product_name" value="{{ old('product_name') }}" name="product_name" type="text" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="image" class="col-3 col-form-label">Ảnh</label>
                <div class="col-9">
                    <input type="file" accept="image/*"  value="{{ old('image') }}" id="image" name="image" onchange="loadFile(event)" style="display: none;" >
                    <img height="260px" width="260px" id="output">
                    <label class="form-label" for="image" style="cursor: pointer;">
                        <p class="btn btn-outline-success"><i class="fa fa-upload"></i></p>
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <label for="price" class="col-3 col-form-label">Giá</label>
                <div class="col-9">
                    <input id="price" name="price" value="{{ old('price') }}" type="number" min="0" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="discount" class="col-3 col-form-label">Khuyến mãi (%)</label>
                <div class="col-9">
                    <input id="discount" name="discount" value="{{ old('discount') }}" min="0" max="100" type="number" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="category_id" class="col-3 col-form-label">Danh mục sản phẩm</label>
                <div class="col-9">
                    <select id="category_id" name="category_id" class="custom-select">
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="quantity" class="col-3 col-form-label">Số lượng</label>
                <div class="col-9">
                    <input id="quantity" name="quantity" value="{{ old('quantity') }}" type="number" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="description" class="col-3 col-form-label">Mô tả</label>
                <div class="col-9">
                      <textarea id="description"  name="description" cols="80" rows="10">{{ old('description') }}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <label for="fileUpload" class="col-3 col-form-label">Ảnh thumbnail</label>
                <div class="col-9">
                    <input id="fileUpload" type="file" name="fileUpload[]" multiple />
                    <div id="image-holder"></div>
                </div>
            </div>

            <div class="form-group row">
                <div class="offset-3 col-9">
                    <button type="submit" class="btn btn-primary">Thêm sản phẩm</button>
                </div>
            </div>

        </form>
    </section>
@endsection
@section('script')
    {{-- js upload 1 ảnh --}}
    <script>
        var loadFile = function(event)
        {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>


    {{-- ckeditor --}}
    <script src="{{asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{asset('ckfinder/ckfinder.js') }}"></script>

    <script>
        CKEDITOR.replace( 'description' );
    </script>


    {{-- Upload nhiều ảnh--}}
    <script>
        $("#fileUpload").on('change', function () {
            //Get count of selected files
            var countFiles = $(this)[0].files.length;
            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $("#image-holder");
            image_holder.empty();

            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg")
            {
                if (typeof (FileReader) != "undefined")
                {
                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++)
                    {
                        var reader = new FileReader();
                        reader.onload = function (e)
                        {
                            $("<img />", { "src": e.target.result, "class": "thumb-image" }).appendTo(image_holder);
                        }
                        image_holder.show();
                        reader.readAsDataURL($(this)[0].files[i]);
                    }
                }
                else
                {
                    alert("This browser does not support FileReader.");
                }
            }
            else
            {
                alert("Pls select only images");
            }
        });
    </script>
@endsection



