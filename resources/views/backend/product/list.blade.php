@extends('backend.layout.master')
@section('title','Danh Sách Sản Phẩm')

@section('content')
  <div class="container">
      <nav class="navbar navbar-light bg-light">
          <a class="btn btn-outline-primary" href="{{ route('admin.product.create') }}">Thêm sản phẩm</a>
          <form class="form-inline">
              <input class="form-control mr-sm-2" type="search" placeholder="Nhập tên sản phẩm" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                  <i class="fa fa-search"></i>
              </button>
          </form>
      </nav>
      @if (Session::has('success'))
          <div class="alert alert-success alert-dismissible" role="alert">
              {{Session::get('success')}}
          </div>
      @endif
      <section style="margin-top: 20px" class="table-responsive">
          <table class="table table-hover">
              <thead class="thead-light">
              <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tên</th>
                  <th scope="col">Ảnh</th>
                  <th scope="col">Giá</th>
                  <th scope="col">Khuyến mãi</th>
                  <th scope="col">Danh Mục</th>
                  <th scope="col">Số lượng</th>
                  <th scope="col">Mô tả</th>
                  <th scope="col">Hành động</th>
              </tr>
              </thead>
              <tbody>
              @foreach($products as $key => $product)
              <tr>
                  <th scope="row">{{ $key+1 }}</th>
                  <td>{{$product->product_name}}</td>
                  <td>
                      <img src="{{asset('images')}}/product_image/{{$product->image}}" width="100px" height="80px" alt="image">
                  </td>
                  <td>{{ number_format($product->price) }}</td>
                  <td>{{ $product->discount }}%</td>
                  <td>
                      @foreach($categories as $category)
                          @if($product->category_id == $category->id)
                          {{ $category->category_name }}
                          @endif
                      @endforeach
                  </td>
                  <td>{{ $product->quantity }}</td>
                  <td>{!! Str::limit($product->description,40) !!}</td>
                  <td>
                      <a href="{{ route('admin.product.edit',$product->id) }}"><i class="fa fa-tools"></i></a> -
                      <a href="{{ route('admin.product.destroy',$product->id) }}"><i class="fa fa-trash-alt"></i></a>
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
      </section>
  </div>
@endsection
@section('script')
    <script>
        setInterval(function(){
            $('.alert-success').hide(2000);
        }, 3000);
    </script>
@endsection





