@extends('backend.layout.master')
@section('title','Danh Sách Danh Mục')
@section('content')
   <div class="container">
       <section class="mb-5 row">
           <div class="col-6">
               <form class="form-inline" method="post"  action="{{ route('admin.category.create') }}">
                   @csrf
                   <div class="form-group mx-sm-3">
                       <label for="category_name" class="sr-only">Tên Danh Mục Sản Phẩm</label>
                       <input type="text" class="form-control" name="category_name" id="category_name" placeholder="Tên Danh Mục">
                   </div>
                   <button type="submit" class="btn btn-primary" >Thêm</button>
               </form>
           </div>
           <div class="col-6 warning-alert">
               @if (Session::has('success'))
                   <div class="alert alert-success alert-dismissible" role="alert">
                       {{Session::get('success')}}
                   </div>
               @endif
           </div>
       </section>
       <section>
           <table class="table table-hover" >
               <thead>
               <tr>
                   <th scope="col">#</th>
                   <th scope="col">Tên Danh Mục</th>
                   <th>Hành Động</th>
               </tr>
               </thead>
               @foreach($category as $key => $value)
               <tr id="category_table">
                   <th scope="row">{{ $key += 1 }}</th>
                   <td>{{ $value->category_name }}</td>
                   <td>
                       <a type="button" data-toggle="modal" data-target="#edit-{{$value->id }}"><i class="fa fa-tools"></i></a> -
                       <a href="{{ route('admin.category.destroy',$value->id) }}"><i class="fa fa-trash-alt"></i></a>

                       <!-- Modal -->
                       <form method="post" action="{{ route('admin.category.update',$value->id ) }}">
                           @csrf
                           <div class="modal fade" id="edit-{{$value->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                               <div class="modal-dialog">
                                   <div class="modal-content">
                                       <div class="modal-body">
                                           <label>Tên Danh Mục</label>
                                           <input type="text" class="form-control" name="category_name" value="{{ $value->category_name }}">
                                       </div>
                                       <div class="modal-footer">
                                           <button type="submit" class="btn btn-primary">Lưu Thay Đổi</button>
                                           <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy Bỏ</button>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </form>
                   </td>
               </tr>
               @endforeach
           </table>
       </section>
   </div>
@endsection
@section('script')
    <script>
        setInterval(function(){
            $('.warning-alert').hide(2000);
        }, 3000);
    </script>
@endsection


