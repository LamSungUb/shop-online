<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <base href="{{ asset('Admin') }}">
    <!-- Custom fonts for this template-->
    <link href="Admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="Admin/css/sb-admin-2.min.css" rel="stylesheet">
    @yield('css')
    <style>
        .fa-tools {
            color: #2e59d9;
        }
        .fa-trash-alt {
            color: #e12717;
        }
    </style>
</head>

<body id="page-top">
    <div id="wrapper">
        @include('backend.partitions.slidebar')
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- header -->
                @include('backend.partitions.header')
                <!-- end-header -->
                <!-- content -->
                @yield('content')
                <!-- end-content -->
            </div>
            <!-- End of Main Content -->
            <!-- footer -->
            @include('backend.partitions.footer')
            <!-- end-footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

<!-- Bootstrap core JavaScript-->
<script src="Admin/vendor/jquery/jquery.min.js"></script>
<script src="Admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="Admin/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="Admin/js/sb-admin-2.min.js"></script>
<!-- Page level plugins -->
{{--<script src="Admin/vendor/chart.js/Chart.min.js"></script>--}}
<!-- Page level custom scripts -->
<script src="Admin/js/demo/chart-area-demo.js"></script>
<script src="Admin/js/demo/chart-pie-demo.js"></script>
<script type='text/javascript' src='https://code.jquery.com/jquery-3.3.1.js'></script>
<script type='text/javascript' src='https://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>

@yield('script')
</body>

</html>
