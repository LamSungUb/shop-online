@extends('backend.layout.template')
@section('title','Đăng Ký Admin')
@section('content')
    <div class="container">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Tạo Tài Khoản!</h1>
                            </div>
                            <form method="post" action="{{ route('admin.post.register') }}" class="user" >
                                @csrf

                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            <span class="sr-only" style="font-size: 35px">Close</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control form-control-user"  placeholder="Họ Và Tên">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control form-control-user"  placeholder="Email">
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" name="password" class="form-control form-control-user" placeholder="Mật Khẩu">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" name="password_confirmation " class="form-control form-control-user" placeholder="Nhập Lại Mật Khẩu ">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Đăng Ký Tài Khoản
                                </button>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="{{ url('admin.login') }}">Đã Có Tài Khoản? Đăng Nhập!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection


