<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
</head>
<body>
{{ $product = "1" }}

<section style="margin: 50px 0px; text-align: center">
    <input type="checkbox" {{ $product == 1 ? 'checked' : "" }}  id="status" data-toggle="toggle" data-onstyle="success" data-offstyle="danger" >
    <input type="hidden" value="active" id="value">
</section>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script>
    $("document").ready(function () {
        $("#status").change(function () {
            if($(this).prop("checked")){
                $("#value").val("1");
            }else{
                $("#value").val("0");
            }

            var status =  $("#value").val();

            console.log(status);
        });
    });
</script>
</body>
</html>
