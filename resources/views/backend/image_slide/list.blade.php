@extends('backend.layout.master')
@section('title','Danh Sách Danh Mục')
@section('content')
    <div class="container">
        @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{Session::get('success')}}
            </div>
        @endif
        <div class="mb-3">
            <a class="btn-outline-primary btn" href="{{ route('admin.slide.create') }}">Thêm slide</a>
        </div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Ảnh</th>
                <th scope="col">Active</th>
                <th scope="col">Hành Động</th>
            </tr>
            </thead>
            <tbody>
            @foreach($slides as $key => $slide)
            <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>
                    <img width="192" height="80" src="{{ asset('Images') }}/slide/{{ $slide->image }}" alt="">
                </td>
                <td>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input"  {{ $slide->active == 1 ? "checked" : "" }} id="customSwitch{{ $slide->id }}"  onchange="return customSwitch(this,{{$slide->id}})" >
                        <input type="hidden" value="active" id="value">
                        <label class="custom-control-label" for="customSwitch{{ $slide->id }}"> Off / On</label>
                    </div>
                </td>
                <td>
                    <a href="{{ route('admin.slide.destroy',$slide->id) }}"><i class="fa fa-trash-alt"></i></a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('script')
    <script>
        setInterval(function(){
            $('.alert-success').hide(2000);
        }, 3000);
    </script>

    <script>
        function customSwitch(e,id)
        {
            if($(e).prop("checked"))
            {
                $("#value").val("1");
            }
            else
            {
                $("#value").val("0");
            }
            var status =  $("#value").val();

            $.ajax({
                url  : "{{ route('admin.slide.active') }}" ,
                type : "GET",
                data :
                    {
                        id: id,
                        active : status,
                    }
            });
        }
    </script>
@endsection



