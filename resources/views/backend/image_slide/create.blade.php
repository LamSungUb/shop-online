@extends('backend.layout.master')
@section('title','Danh Sách Danh Mục')
@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only" style="font-size: 35px">Close</span>
                </button>
            </div>
        @endif
        <form method="post" action="{{ route('admin.slide.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="image" class="col-2 col-form-label">Slider</label>
                <div class="col-8 mt-1 mb-1">
                    <input type="file" accept="image/*"  id="image" name="slide" onchange="loadFile(event)" style="display: none;" >
                    <img height="240px" width="576px" id="output">
                    <label class="form-label mt-1" for="image" style="cursor: pointer;">
                        <p class="btn btn-outline-success"><i class="fa fa-upload"></i></p>
                    </label>
                </div>
                <div class="col-2">
                    <button type="submit" class="btn btn-outline-success ">Thêm</button>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('script')
    <script>
        var loadFile = function(event)
        {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
@endsection


