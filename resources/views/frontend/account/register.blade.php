@extends('frontend.account.layout')
@section('title','Đăng Ký')
@section('content')
    <form method="post" action="{{ route('site.post.register') }}">
        @csrf
        @if (Session::has('warning'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{Session::get('warning')}}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only" style="font-size: 35px">Close</span>
                </button>
            </div>
        @endif
        <div class="form-group input-group-lg">
            <input type="text" name="name" class="form-control rounded-pill" placeholder="Họ Và Tên">
        </div>
        <div class="form-group input-group-lg">
            <input type="email"  name="email" class="form-control rounded-pill" placeholder="Địa Chỉ Email">
        </div>
        <div class="row">
            <div class="form-group input-group-lg col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <input type="password"  name="password" class="form-control rounded-pill" placeholder="Mật Khẩu">
            </div>
            <div class="form-group input-group-lg  col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <input type="password" name="password_confirmation" class="form-control rounded-pill" placeholder="Nhập Lại Mật Khẩu">
            </div>
        </div>


        <button type="submit" class="btn submit rounded-pill  w-100">Tạo Tài Khoản</button>

        <hr>

        <div class="form-group">
            <a href="" class="btn rounded-pill google w-100"> <i class="fa fa-google"></i> Đăng Ký Bằng Google</a>
        </div>

        <div class="form-group">
            <a href="" class="btn rounded-pill facebook w-100"> <i class="fa fa-facebook-f"></i> Đăng Ký Bằng Facebook</a>
        </div>

        <hr>
        <div class="form-group text-center">
            <a href="{{ route('site.login') }}">Đã Có Tài Khoản? Đăng Nhập!</a>
        </div>
    </form>
@endsection
