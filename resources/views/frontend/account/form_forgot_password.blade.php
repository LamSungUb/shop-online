@extends('frontend.account.layout')
@section('title','Đổi Mật Khẩu')
@section('content')
    <form method="post" action="{{ route('site.verify.forgot.password') }}">
        @csrf
        @if (Session::has('warning'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{Session::get('warning')}}
            </div>
        @endif
        @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{Session::get('success')}}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only" style="font-size: 35px">Close</span>
                </button>
            </div>
        @endif
        <input type="hidden" value="{{$email}}" name="email">
        <input type="hidden" value="{{$code}}" name="code">

        <div class="form-group input-group-lg ">
            <input type="password" name="password" class="form-control rounded-pill" placeholder="Mật Khẩu Mới">
        </div>

        <div class="form-group input-group-lg ">
            <input type="password" name="password_confirmation" class="form-control rounded-pill" placeholder="Nhập Lại Mật Khẩu Mới">
        </div>

        <button type="submit" class="btn submit rounded-pill  w-100">Đổi Mật Khẩu</button>
        <br>
    </form>
@endsection
