@extends('frontend.account.layout')
@section('title','Đăng Nhập')
@section('content')
    <form method="post" action="{{ route('site.post.login') }}" >
        @csrf
        @if (Session::has('warning'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{Session::get('warning')}}
            </div>
        @endif
        @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{Session::get('success')}}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only" style="font-size: 35px">Close</span>
                </button>
            </div>
        @endif
        <div class="form-group input-group-lg">
            <input type="email" name="email" class="form-control rounded-pill" placeholder="Địa Chỉ Email">
        </div>

        <div class="form-group input-group-lg">
            <input type="password" name="password" class="form-control rounded-pill" placeholder="Mật Khẩu">
        </div>

        <div class="form-group form-check ">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Nhớ Tài Khoản</label>
        </div>

        <button type="submit" class="btn submit rounded-pill  w-100">Đăng Nhập Tài Khoản</button>

        <hr>

        <div class="form-group">
            <a href="" class="btn rounded-pill google w-100"> <i class="fa fa-google"></i> Đăng Nhập Bằng Google</a>
        </div>

        <div class="form-group">
            <a href="" class="btn rounded-pill facebook w-100"> <i class="fa fa-facebook-f"></i> Đăng Nhập Bằng Facebook</a>
        </div>

        <hr>
        <div class="form-group text-center">
            <div class="mb-2">
                <a href="{{ route('site.forgot_password') }}" target="_blank"> Quên Mật Khẩu?</a>
            </div>
            <div>
                <a href="{{ route('site.register') }}">Đăng Ký Tạo Tài Khoản!</a>
            </div>
        </div>
    </form>
@endsection
