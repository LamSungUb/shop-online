@extends('frontend.account.layout')
@section('title','Quên Mật Khẩu')
@section('content')
    <form method="post" action="{{ route('site.post.forgot_password') }}">
        @csrf
        <h4>Bạn vui lòng điền tài khoản Gmail để lấy lại mật khẩu</h4>
        <br>
        @if (Session::has('success'))
            <div class="alert alert-success  mt-1" role="alert">
                {{Session::get('success')}}
            </div>
        @endif
        @if (Session::has('warning'))
            <div class="alert alert-danger  mt-1" role="alert">
                {{Session::get('warning')}}
            </div>
        @endif
        <div class="form-group input-group-lg">
            <input type="email" name="email" class="form-control rounded-pill" placeholder="Địa Chỉ Email">
        </div>
        <button type="submit" class="btn submit rounded-pill  w-100">Xác Nhận Email</button>
    </form>
@endsection
