@extends('frontend.layout.master')
@section('title','Chi Tiết Blog')
@section('content')
    <x-service></x-service>
    <main class="ps-main">
        <div class="ps-contact ps-contact--2 ps-section pt-80 pb-80">
            <div class="ps-container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                        <div class="ps-section__header pt-50">
                            <h2 class="ps-section__title" data-mask="Contact">- Get in touch</h2>
                            <form class="ps-contact__form" action="do_action" method="post">
                                <div class="form-group">
                                    <label>Name <sub>*</sub></label>
                                    <input class="form-control" type="text" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Email <sub>*</sub></label>
                                    <input class="form-control" type="email" placeholder="">
                                </div>
                                <div class="form-group mb-25">
                                    <label>Your Message <sub>*</sub></label>
                                    <textarea class="form-control" rows="6"></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="ps-btn">Send Message<i class="ps-icon-next"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                        <div class="ps-contact__block" data-mh="contact-block">
                            <header>
                                <h3>Ireland  <span> Dublin</span></h3>
                            </header>
                            <footer>
                                <p><i class="fa fa-map-marker"></i> 19C Trolley Square  Wilmington, DE 19806</p>
                                <p><i class="fa fa-envelope-o"></i><a href="mailto@supportShoes@shoes.net">supportShoes@shoes.net</a></p>
                                <p><i class="fa fa-phone"></i> ( +84 ) 9892 2324  -  9401 123 003</p>
                            </footer>
                        </div>
                        <div>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29793.98038438121!2d105.8194541087431!3d21.02277876319995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab9bd9861ca1%3A0xe7887f7b72ca17a9!2zSMOgIE7hu5lpLCBIb8OgbiBLaeG6v20sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1596718831070!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection


