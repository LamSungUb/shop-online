@extends('frontend.layout.master')
@section('title','Chi Tiết Sản Phẩm')
@php
    use App\Models\Comment;
@endphp
@section('content')
    <main class="ps-main">
        <x-service></x-service>
        <div class="test">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-product--detail pt-60">
            <div class="ps-container">
                <div class="row">
                    <div class="col-lg-10 col-md-12 col-lg-offset-1">
                        <div class="ps-product__thumbnail">
                            <div class="ps-product__preview">
                                <div class="ps-product__variants">
                                    <div class="item">
                                        <img src="{{ asset('Images') }}/product_image/{{ substr($product->image,28) }}" alt="">
                                    </div>
                                    @foreach($thumbnails as $thumbnail)
                                    <div class="item">
                                        <img src="{{ asset('Images') }}/product_thumbnail/{{ $thumbnail->thumbnail_name }}" alt="">
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="ps-product__image">
                                <div class="item">
                                    <img class="zoom" src="{{ asset('Images') }}/product_image/{{ substr($product->image,28) }}" alt="" data-zoom-image="{{ asset('Images') }}/product_image/{{ substr($product->image,28) }}">
                                </div>
                                @foreach($thumbnails as $thumbnail)
                                <div class="item">
                                    <img class="zoom" src="{{ asset('Images') }}/product_thumbnail/{{ $thumbnail->thumbnail_name }}" alt="" data-zoom-image="{{ asset('Images') }}/product_thumbnail/{{ $thumbnail->thumbnail_name }}">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="ps-product__info">
                            <div class="row">
                                <div class="col-6">
                                    <i class="fa fa-star fa-2x" style="color:#FFBA00 "></i>
                                    <i class="fa fa-star fa-2x" style="color:#FFBA00 "></i>
                                    <i class="fa fa-star fa-2x" style="color:#FFBA00 "></i>
                                    <i class="fa fa-star fa-2x" style="color:#FFBA00 "></i>
                                    <i class="fa fa-star fa-2x" style="color:#FFBA00 "></i>
                                </div>
                                <div class="col-6">
                                    <a href="#">(Đọc tất cả 8 danh giá)</a>
                                </div>
                            </div>
                            <h1>{{ $product->product_name }}</h1>
                            <p class="ps-product__category"><a href="#"> {{ $category->category_name }}</a></p>
                            <h3 class="ps-product__price">{{ number_format($product->price*$product->discount/100) }} đ  <del>{{ number_format($product->price) }} đ</del></h3>
                            <div class="ps-product__block ps-product__size">
                                <p class="ps-select ">
                                    Tình Trạng : Còn hàng
                                </p>
                            </div>
                            <div class="ps-product__block ps-product__size">
                                <p class="ps-select ">
                                    Chọn Số Lượng
                                </p>
                                <div class="form-group">
                                    <input class="form-control" type="number" value="1">
                                </div>
                            </div>
                            <div class="ps-product__shopping"><a class="ps-btn mb-10" href="cart.html">Thêm vào giỏ hàng<i class="ps-icon-next"></i></a>
                                <div class="ps-product__actions"><a class="mr-10" href="whishlist.html"><i class="ps-icon-heart"></i></a><a href="compare.html"><i class="ps-icon-share"></i></a></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ps-product__content mt-50">
                            <ul class="tab-list" role="tablist">
                                <li class="active">
                                    <a href="#tab_01" aria-controls="tab_01" role="tab" data-toggle="tab">Thông tin chi tiết</a>
                                </li>
                                <li>
                                    <a href="#tab_02" aria-controls="tab_02" role="tab" data-toggle="tab">Đánh giá</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content mb-60">
                            <div class="tab-pane active" role="tabpanel" id="tab_01">
                                {!! $product->description !!}
                            </div>
                            @if(Auth::guard('customers')->check())
                                <div class="tab-pane" role="tabpanel" id="tab_02">
                                <p class="mb-20">
                                    Đánh giá sản phẩm
                                    <strong>{{ $product->product_name }}</strong>
                                </p>
                                @foreach($comments as $comment)
                                <div class="ps-review">
                                    <div class="ps-review__thumbnail">
                                        <img src="{{ asset('Images/user_image/user.png') }}" alt= "Customer Image">
                                    </div>
                                    <div class="ps-review__content">
                                        <header>
                                            <p class="pr-5">
                                                <a href="">{{ Comment::getCustomerByCommentId($comment->id)->name }}</a>
                                            </p>
                                            <p>
                                                @for($i = 0; $i < $rating->point; $i++)
                                                <i class="fa fa-star " style="color:#FFBA00 "></i>
                                                @endfor
                                                @for($i = 0; $i < 5- $rating->point; $i++)
                                                    <i class="fa fa-star "></i>
                                                @endfor
                                            </p>
                                            <p class="float-right">{{$comment->created_at->diffForHumans($now)}}</p>
                                        </header>
                                        <p>{{ $comment->content }}</p>
                                        <div class="float-right">
                                            <a href="" class="small">Sửa</a> /<a href="" class="small"> Xóa</a>
                                        </div>
                                    </div>

                                </div>
                                @endforeach
                                <form class="ps-product__review" action="{{ route('site.comment',['productID' => $product->id]) }}" method="post">
                                    @csrf
                                    <div class="row">
                                        @if($rating->customer_id != Auth::guard('customers')->user()->id)
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                            <div class="form-group">
                                                <label>Đánh giá xếp hạng của bạn:<span></span></label>
                                                <select class="ps-rating" name="rating">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-12 ">
                                            <div class="form-group">
                                                <label>Bình luận của bạn:</label>
                                                <textarea class="form-control" rows="6"  name="comment"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button class="ps-btn ps-btn--sm" type="submit">
                                                    Gửi<i class="ps-icon-next"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @else
                                <div class="tab-pane" role="tabpanel" id="tab_02">
                                    <p class="mb-20">
                                        Đánh giá sản phẩm
                                        <strong>{{ $product->product_name }}</strong>
                                    </p>
                                    @foreach($comments as $comment)
                                        <div class="ps-review">
                                            <div class="ps-review__thumbnail">
                                                <img src="{{ asset('Images/user_image/user.png') }}" alt= "Customer Image">
                                            </div>
                                            <div class="ps-review__content">
                                                <header>
                                                    <p class="pr-5">
                                                        <a href="">{{ Comment::getCustomerByCommentId($comment->id)->name }}</a>
                                                    </p>
                                                    <p>
                                                        @for($i = 0; $i < $rating->point; $i++)
                                                            <i class="fa fa-star " style="color:#FFBA00 "></i>
                                                        @endfor
                                                        @for($i = 0; $i < 5- $rating->point; $i++)
                                                            <i class="fa fa-star "></i>
                                                        @endfor
                                                    </p>
                                                    <p class="float-right">{{$comment->created_at->diffForHumans($now)}}</p>
                                                </header>
                                                <p>{{ $comment->content }}</p>
                                                <div class="float-right">
                                                    <a href="" class="small">Sửa</a> /<a href="" class="small"> Xóa</a>
                                                </div>
                                            </div>

                                        </div>
                                    @endforeach
                                    <label>Bình luận của bạn:</label>
                                    <a class="btn btn-success" href="{{ route('site.login') }}"> Vui lòng đăng nhập để đánh giá</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

