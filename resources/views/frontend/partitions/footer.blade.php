<footer class="ps-footer bg--cover" data-background="images/background/parallax.jpg">
    <div class="ps-footer__copyright">
        <div class="ps-container">
            <div class="row text-center">
                <div class="col-12">
                    <p> <a href="#">ONLINE SHOP</a>&copy; My Website 2020</p>
                </div>
            </div>
        </div>
    </div>
</footer>
