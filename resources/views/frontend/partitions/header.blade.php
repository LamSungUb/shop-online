<header class="header">
    <div class="header__top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-8 col-sm-6 col-xs-12 ">
                    <p>Shop Online  -  Hotline: 0336789999</p>
                </div>
                <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 ">
                    <div class="header__actions">
                        @if(Auth::guard('customers')->check())
                            <a type="button" class="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown">
                                {{ Auth::guard('customers')->user()->name }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <a href="{{ route('site.change_password') }}" class="dropdown-item" type="button">Đổi Mật Khẩu</a>
                                <a href="{{ route('site.logout') }}" class="dropdown-item" type="button">Đăng Xuất</a>
                            </div>
                        @else
                            <a href="{{ route('site.login') }}">Đăng Nhập & Đăng Ký</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="navigation">
        <div class="container-fluid">
            <div class="navigation__column left">
                <div class="header__logo">
                    <a class="ps-logo" href="{{ route('index') }}">
                        <img src="{{asset('Site')}}/images/logo.png" alt="logo">
                    </a>
                </div>
            </div>
            <div class="navigation__column center">
                <ul class="main-menu menu">
                    <li class="menu-item">
                        <a href="{{ route('index') }}">Trang Chủ</a>
                    </li>
                    <li class="menu-item menu-item-has-children dropdown">
                        <a href="#">Sản Phẩm</a>
                        <ul class="sub-menu">
                            @foreach($categories as $category)
                                <li class="menu-item">
                                    <a href="contact-us.html">{{$category->category_name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="menu-item ">
                        <a href="#">Tin Tức</a>
                    </li>
                    <li class="menu-item ">
                        <a href="#">Liên Hệ</a>
                    </li>
                </ul>
            </div>
            <div class="navigation__column right">
                <form class="ps-search--header" action="do_action" method="post">
                    <input class="form-control" type="text" placeholder="Search Product…">
                    <button><i class="ps-icon-search"></i></button>
                </form>
                <div class="ps-cart"><a class="ps-cart__toggle" href="#"><span><i>20</i></span><i class="ps-icon-shopping-cart"></i></a>
                    <div class="ps-cart__listing">
                        <div class="ps-cart__content">
                            <div class="ps-cart-item"><a class="ps-cart-item__close" href="#"></a>
                                <div class="ps-cart-item__thumbnail"><a href="product-detail.html"></a><img src="Site/images/cart-preview/1.jpg" alt=""></div>
                                <div class="ps-cart-item__content"><a class="ps-cart-item__title" href="product-detail.html">Amazin’ Glazin’</a>
                                    <p><span>Quantity:<i>12</i></span><span>Total:<i>£176</i></span></p>
                                </div>
                            </div>
                            <div class="ps-cart-item"><a class="ps-cart-item__close" href="#"></a>
                                <div class="ps-cart-item__thumbnail"><a href="product-detail.html"></a><img src="Site/images/cart-preview/2.jpg" alt=""></div>
                                <div class="ps-cart-item__content"><a class="ps-cart-item__title" href="product-detail.html">The Crusty Croissant</a>
                                    <p><span>Quantity:<i>12</i></span><span>Total:<i>£176</i></span></p>
                                </div>
                            </div>
                            <div class="ps-cart-item"><a class="ps-cart-item__close" href="#"></a>
                                <div class="ps-cart-item__thumbnail"><a href="product-detail.html"></a><img src="Site/images/cart-preview/3.jpg" alt=""></div>
                                <div class="ps-cart-item__content"><a class="ps-cart-item__title" href="product-detail.html">The Rolling Pin</a>
                                    <p><span>Quantity:<i>12</i></span><span>Total:<i>£176</i></span></p>
                                </div>
                            </div>
                        </div>
                        <div class="ps-cart__total">
                            <p>Number of items:<span>36</span></p>
                            <p>Item Total:<span>£528.00</span></p>
                        </div>
                        <div class="ps-cart__footer"><a class="ps-btn" href="cart.html">Check out<i class="ps-icon-arrow-left"></i></a></div>
                    </div>
                </div>
                <div class="menu-toggle"><span></span></div>
            </div>
        </div>
    </nav>
</header>
