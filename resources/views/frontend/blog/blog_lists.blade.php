@extends('frontend.layout.master')
@section('title','Chi Tiết Blog')
@section('content')
    <x-service></x-service>
    <main class="ps-main">
        <div class="ps-blog-grid pt-80 pb-80">
            <div class="ps-container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
                        <div class="ps-post--2">
                            <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="#"></a><img src="Site/images/blog/4.jpg" alt=""></div>
                            <div class="ps-post__container">
                                <header class="ps-post__header"><a class="ps-post__title" href="blog-detail.html">Unpacking the Breaking 2 Race Strategy</a>
                                    <p>Posted by <a href="blog-grid.html">Alena Studio</a> on August 17, 2016  in <a href="blog-grid.html">Men Shoes</a> , <a href="blog-grid.html">Stylish</a></p>
                                </header>
                                <div class="ps-post__content">
                                    <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further, Support Sports Bra has a compression fit and V-back straps that give you medium support and full range of motion during your workout….</p>
                                </div>
                                <footer class="ps-post__footer"><a class="ps-post__morelink" href="#">READ MORE<i class="ps-icon-arrow-left"></i></a>
                                    <div class="ps-post__actions"><span><i class="fa fa-comments"></i> 23 Comments</span><span><i class="fa fa-heart"></i>  likes</span>
                                        <div class="ps-post__social"><i class="fa fa-share-alt"></i><a href="#">Share</a>
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                        <div class="ps-post--2">
                            <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="#"></a><img src="Site/images/blog/5.jpg" alt=""></div>
                            <div class="ps-post__container">
                                <header class="ps-post__header"><a class="ps-post__title" href="blog-detail.html">Leverage agile frameworks to provide a robust synopsis</a>
                                    <p>Posted by <a href="blog-grid.html">Alena Studio</a> on August 17, 2016  in <a href="blog-grid.html">Men Shoes</a> , <a href="blog-grid.html">Stylish</a></p>
                                </header>
                                <div class="ps-post__content">
                                    <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further, Support Sports Bra has a compression fit and V-back straps that give you medium support and full range of motion during your workout….</p>
                                </div>
                                <footer class="ps-post__footer"><a class="ps-post__morelink" href="#">READ MORE<i class="ps-icon-arrow-left"></i></a>
                                    <div class="ps-post__actions"><span><i class="fa fa-comments"></i> 23 Comments</span><span><i class="fa fa-heart"></i>  likes</span>
                                        <div class="ps-post__social"><i class="fa fa-share-alt"></i><a href="#">Share</a>
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                        <div class="ps-post--2">
                            <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="#"></a><img src="Site/images/blog/3.jpg" alt=""></div>
                            <div class="ps-post__container">
                                <header class="ps-post__header"><a class="ps-post__title" href="blog-detail.html">Nike’s Latest Football Cleat Breaks the Mold</a>
                                    <p>Posted by <a href="blog-grid.html">Alena Studio</a> on August 17, 2016  in <a href="blog-grid.html">Men Shoes</a> , <a href="blog-grid.html">Stylish</a></p>
                                </header>
                                <div class="ps-post__content">
                                    <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further, Support Sports Bra has a compression fit and V-back straps that give you medium support and full range of motion during your workout….</p>
                                </div>
                                <footer class="ps-post__footer"><a class="ps-post__morelink" href="#">READ MORE<i class="ps-icon-arrow-left"></i></a>
                                    <div class="ps-post__actions"><span><i class="fa fa-comments"></i> 23 Comments</span><span><i class="fa fa-heart"></i>  likes</span>
                                        <div class="ps-post__social"><i class="fa fa-share-alt"></i><a href="#">Share</a>
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                        <div class="ps-post--2">
                            <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="#"></a><img src="Site/images/blog/4.jpg" alt=""></div>
                            <div class="ps-post__container">
                                <header class="ps-post__header"><a class="ps-post__title" href="blog-detail.html">Breaking Down the Black Rose Tech Fleece Collection</a>
                                    <p>Posted by <a href="blog-grid.html">Alena Studio</a> on August 17, 2016  in <a href="blog-grid.html">Men Shoes</a> , <a href="blog-grid.html">Stylish</a></p>
                                </header>
                                <div class="ps-post__content">
                                    <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further, Support Sports Bra has a compression fit and V-back straps that give you medium support and full range of motion during your workout….</p>
                                </div>
                                <footer class="ps-post__footer"><a class="ps-post__morelink" href="#">READ MORE<i class="ps-icon-arrow-left"></i></a>
                                    <div class="ps-post__actions"><span><i class="fa fa-comments"></i> 23 Comments</span><span><i class="fa fa-heart"></i>  likes</span>
                                        <div class="ps-post__social"><i class="fa fa-share-alt"></i><a href="#">Share</a>
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                        <div class="ps-post--2">
                            <div class="ps-post__thumbnail"><a class="ps-post__overlay" href="#"></a><img src="Site/images/blog/5.jpg" alt=""></div>
                            <div class="ps-post__container">
                                <header class="ps-post__header"><a class="ps-post__title" href="blog-detail.html">The Shoes Athletes Say Will Change the Future of Running</a>
                                    <p>Posted by <a href="blog-grid.html">Alena Studio</a> on August 17, 2016  in <a href="blog-grid.html">Men Shoes</a> , <a href="blog-grid.html">Stylish</a></p>
                                </header>
                                <div class="ps-post__content">
                                    <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further, Support Sports Bra has a compression fit and V-back straps that give you medium support and full range of motion during your workout….</p>
                                </div>
                                <footer class="ps-post__footer"><a class="ps-post__morelink" href="#">READ MORE<i class="ps-icon-arrow-left"></i></a>
                                    <div class="ps-post__actions"><span><i class="fa fa-comments"></i> 23 Comments</span><span><i class="fa fa-heart"></i>  likes</span>
                                        <div class="ps-post__social"><i class="fa fa-share-alt"></i><a href="#">Share</a>
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                        <div class="mt-30">
                            <div class="ps-pagination">
                                <ul class="pagination">
                                    <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">...</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                        <x-related_blogs></x-related_blogs>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

