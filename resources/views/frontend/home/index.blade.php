@extends('frontend.layout.master')
@section('title','Trang Chủ')
@section('content')
    <main class="ps-main">
        <x-service></x-service>
        <x-slider></x-slider>
        <x-feature></x-feature>
        <x-banner></x-banner>
        <x-sale></x-sale>
        <x-blog></x-blog>
    </main>
@endsection
@section('script')
    <script>
        function getProductByCategory(e) {
            var id = $(e).data('id');
            $.ajax({
                url: "{{ route('site.ajax.product') }}",
                method: 'GET',
                data: {
                    cate_id : id,
                },
                success: function (data) {
                    $('div#ajax').empty();
                    $('div#ajax').html(data);
                }
            })
        }
    </script>
@endsection

