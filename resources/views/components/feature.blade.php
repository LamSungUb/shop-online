<div class="ps-section--features-product ps-section masonry-root pt-100 pb-100">
    <div class="ps-container">
        <div class="ps-section__header mb-50">
            <h3 class="ps-section__title">- Sản Phẩm mới nhất</h3>
            <ul class="ps-masonry__filter">
                <li class="current">
                    <a href="#" data-filter="*">All</a>
                </li>
                @foreach($categories as $category)
                    <li>
                        <a data-filter=".{{ $category->id }}" style="cursor: pointer" >
                            {{ $category->category_name }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="ps-section__content pb-50">
            <div class="masonry-wrapper" data-col-md="4" data-col-sm="2" data-col-xs="1" data-gap="30" data-radio="100%">
                <div class="ps-masonry" >
                    @foreach($products as $product)
                    <div class="grid-item {{ $product->category_id }} ">
                        <div class="grid-item__content-wrapper">
                            <div class="ps-shoe mb-30">
                                <div class="ps-shoe__thumbnail">
                                    @if($newProduct($product->id) < 7)
                                        <div class="ps-badge">
                                            <span>New</span>
                                        </div>
                                    @endif
                                    @if($product->discount )
                                        <div class="ps-badge ps-badge--sale ps-badge--2nd">
                                            <span>-{{ $product->discount }}%</span>
                                        </div>
                                    @endif
                                        <a class="ps-shoe__favorite" href="#">
                                            <i class="ps-icon-heart"></i>
                                        </a>
                                        <img src="{{ asset('Images') }}/product_image/{{ substr($product->image,28) }}" alt="">
                                        <a class="ps-shoe__overlay" href="{{ route('site.product.detail',$product->id) }}"></a>
                                </div>
                                <div class="ps-shoe__content">
                                    <div class="ps-shoe__variants">
                                        <div class="ps-shoe__variant normal">
                                            @foreach($thumbnail($product->id) as $value)
                                                @if($value)
                                                    <img src="{{asset('Images') }}/product_thumbnail/{{ $value->thumbnail_name }}" alt="">
                                                @endif
                                            @endforeach
                                        </div>
                                        <select class="ps-rating ps-shoe__rating">
                                            <option value="1">1</option>
                                            <option value="1">2</option>
                                            <option value="1">3</option>
                                            <option value="1">4</option>
                                            <option value="2">5</option>
                                        </select>
                                    </div>
                                    <div class="ps-shoe__detail">
                                        <a class="ps-shoe__name" href="{{ route('site.product.detail',$product->id) }}">{{ $product->product_name }}</a>
                                        <p class="ps-shoe__categories">
                                            <a href="#">{{ $categoryName($product->category_id)->category_name }}</a>
                                        </p>
                                        <span class="ps-shoe__price" style="color: orangered">
                                             {{ number_format($product->price*$product->discount/100) }} đ
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
