<div class="header-services mb-3">
    <div class="ps-services owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="7000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="false" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on">
        <p class="ps-service"><i class="fa fa-star"></i></i><strong>Hàng chính hãng 100%</strong>: Đảm bảo hàng chính hãng hoặc hoàn tiền gấp đôi</p>
        <p class="ps-service"><i class="ps-icon-delivery"></i><strong>Miễn phí vận chuyển</strong>: Giao hàng miễn phí toàn quốc</p>
        <p class="ps-service"><i class="fa fa-thumbs-up"></i></i><strong>7 Ngày hoàn trả miễn phí</strong>: Cam kết dịch vụ hoàn trả trong vòng 7 ngày</p>
    </div>
</div>
