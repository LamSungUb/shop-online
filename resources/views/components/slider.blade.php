<div class="ps-banner">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @for($i = 0; $i < $slides->count(); $i ++)
            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $i }}"  class="{{ $i == 0 ? "active" : '' }}"></li>
            @endfor
        </ol>
        <div class="carousel-inner">
            @foreach($slides as $slide)
                <div  class='carousel-item {{ $slide->active == 1 ? "active" : "" }}' >
                    <img src="{{ asset('Images') }}/slide/{{ $slide->image }}" class="d-block w-100" alt="...">
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
