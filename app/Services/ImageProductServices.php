<?php

namespace App\Services;

use Illuminate\Support\Str;


class ImageProductServices
{
    public static function addImage($addImageProduct)
    {
        $image = Str::random(40).$addImageProduct->getClientOriginalName();

        $addImageProduct->move(public_path('/Images/product_image/'), $image);

        return $image;
    }

    public static function addThumbnail($thumbnail)
    {
        $thumbnail_name = Str::random(40).$thumbnail->getClientOriginalName();

        $thumbnail->move(public_path('Images/product_thumbnail/') , $thumbnail_name);

        return $thumbnail_name;
    }
}
