<?php

namespace App\View\Components;

use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ThumbnailRepository;
use Illuminate\View\Component;

class feature extends Component
{
    public $categories;

    public $products;

    public $new;

    public $categoryName;

    public $thumbnail;


    public function __construct(CategoryRepository $categoryRepository, ProductRepository $productRepository, ThumbnailRepository $thumbnailRepository)
    {
        $this->categories = $categoryRepository->getAll();

        $this->products = $productRepository->getProducts();

        $this->new = $productRepository;

        $this->categoryName = $categoryRepository;

        $this->thumbnail = $thumbnailRepository;

    }


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.feature');
    }

    public function newProduct($id)
    {
        return $this->new->newProducts($id);
    }

    public function categoryName($cateId)
    {
        return $this->categoryName->findById($cateId);
    }

    public function thumbnail($productId)
    {
        return $this->thumbnail->getThumbnailProductId($productId);
    }
}
