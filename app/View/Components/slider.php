<?php

namespace App\View\Components;

use App\Repositories\ImageSildeRepository;
use Illuminate\View\Component;

class slider extends Component
{
    public $slides;

    public function __construct(ImageSildeRepository $imageSildeRepository)
    {
        $this->slides = $imageSildeRepository->showSlide();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.slider');
    }
}
