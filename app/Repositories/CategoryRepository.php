<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Str;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class CategoryRepository.
 */
class CategoryRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Category::class;
    }

    public function save(array $input,$id = null)
    {
        return Category::updateOrCreate(
            ['id' => $id],
            [
                'category_name'      => $input['category_name'],
                'slug'               => Str::slug($input['category_name'], '-'),
                'parent_id'          => $input['parent_id'] ?? 0,
            ]
        );
    }

    public function getAll()
    {
        return Category::get();
    }

    public function findById($id)
    {
        return Category::find($id);
    }

    public function getProductByCategoryId($cateId)
    {
        return Category::find($cateId)->products;
    }
    public function notCategoryId($id)
    {
        return Category::whereNotIn('id',[$id])->get();
    }

    public function destroy(array $id)
    {
        return Category::destroy($id);
    }
}
