<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\Thumbnail;
use Illuminate\Support\Facades\DB;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class ThumbnailRepository.
 */
class ThumbnailRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Thumbnail::class;
    }
    public function getThumbnailProductId($producId)
    {
        return Thumbnail::where('product_id',$producId)->get();
    }

    public function getThumbnailById($id)
    {
        return Thumbnail::where('id',$id)->first();
    }

    public function createProductThumbnail($productId,$thumbnail)
    {
        return Thumbnail::insert([
            "product_id"        => $productId,
            "thumbnail_name"    => $thumbnail,
        ]);
    }

    public function updateProductThumbnail($id,$thumbnail_name)
    {
        return DB::table('thumbnails')
            ->where('id',$id)
            ->update([
                "thumbnail_name"    => $thumbnail_name,
            ]);
    }
    public function deleteProductThumbnail($productId)
    {
        return DB::table('thumbnails')->where('product_id',$productId )->delete();
    }

    public function deleteThumbnail($id)
    {
        return DB::table('thumbnails')->where('id',$id )->delete();
    }
}
