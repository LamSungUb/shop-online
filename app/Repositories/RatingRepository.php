<?php

namespace App\Repositories;

use App\Models\Rating;
use Illuminate\Support\Facades\Auth;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class RatingRepository.
 */
class RatingRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Rating::class;
    }
    public function saveRating($productId,$customer_id,$point)
    {
        return Rating::insert([
            'product_id'        => $productId,
            'customer_id'       => $customer_id,
            'point'             => $point,
            'created_at'        => new \DateTime(),
            'updated_at'        => new \DateTime()
        ]);
    }
    public function getRating($productId)
    {
        return Rating::where('product_id',$productId)->first();
    }
}
