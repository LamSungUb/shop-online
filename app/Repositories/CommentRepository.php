<?php

namespace App\Repositories;

use App\Models\Comment;
use Illuminate\Support\Facades\Auth;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class CommentRepository.
 */
class CommentRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Comment::class;
    }
    public function saveComment($productId,$customer_id,$content)
    {
        return Comment::insert([
            'product_id'        => $productId,
            'customer_id'       => $customer_id,
            'content'           => $content,
            'created_at'        => new \DateTime(),
            'updated_at'        => new \DateTime()
        ]);
    }
    public function getComment($productId)
    {
        return Comment::where('product_id',$productId)
            ->orderBy('created_at','desc')
            ->take(5)
            ->get();
    }
    public function editComment($id,$content)
    {
        return Comment::find($id)->update([
            'content'  => $content,
        ]);
    }

    public function delComment($id)
    {
        return Comment::find($id)->delete();
    }
}
