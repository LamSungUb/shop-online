<?php

namespace App\Repositories;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;


class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }

    public function addProduct(array $input)
    {
        return Product::insertGetId(
            [
                'product_name'  => $input['product_name'],
                'slug'          => Str::slug($input['product_name'], '-'),
                'price'         => $input['price'],
                'discount'      => $input['discount'],
                'category_id'   => $input['category_id'],
                'image'         => $input['image'],
                'quantity'      => $input['quantity'],
                'description'   => $input['description'],
                'created_at'    => new \DateTime(),
                'updated_at'    => new \DateTime()
            ]
        );
    }

    public function getAllProducts()
    {
        return Product::get();
    }

    public function getProducts()
    {
        return Product::orderBy('created_at','DESC')->take(12)->get();
    }

    public function findById($id)
    {
        return Product::find($id);
    }
    public function findCategoryByProductId($id)
    {
        return Product::find($id)->category;
    }
    public function destroy($id)
    {
        return Product::find($id)->delete();
    }

    public function update($id, array $input, $image)
    {
        return Product::updateOrCreate(
            [
                'id' => $id
            ],
            [
                'product_name'  => $input['product_name'],
                'slug'          => Str::slug($input['product_name'], '-'),
                'price'         => $input['price'],
                'discount'      => $input['discount'],
                'category_id'   => $input['category_id'],
                'image'         => $image,
                'quantity'      => $input['quantity'],
                'description'   => $input['description']
            ]
        );
    }

    public function getProductByCategory($cate_id)
    {
        return DB::table('products')
            ->where('category_id',$cate_id)
            ->get();
    }
    public function newProducts($id)
    {
        $product = Product::where('id',$id)->firstOrFail();
        $now = Carbon::now();
        $date = $product->created_at;
        return $now->diffInDays($date);
    }
}
