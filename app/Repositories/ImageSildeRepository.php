<?php

namespace App\Repositories;

use App\Models\ImageSlide;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class ImageSildeRepository.
 */
class ImageSildeRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return ImageSlide::class;
    }

    public function save($slide)
    {
        return DB::table('image_slides')->insert([
            'image'         => $slide,
            'created_at'    => new \DateTime(),
            'updated_at'    => new \DateTime()
        ]);
    }

    public function findById($id)
    {
        return DB::table('image_slides')->find($id);
    }

    public function getAllSlide()
    {
        return DB::table('image_slides')->get();
    }
    public function showSlide()
    {
        return DB::table('image_slides')
            ->orderBy('id','DESC')
            ->take(5)
            ->get();
    }
    public function destroy($id)
    {
        return DB::table('image_slides')
            ->where('id',$id)
            ->delete();
    }

}
