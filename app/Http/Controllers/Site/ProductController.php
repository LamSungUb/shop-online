<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Repositories\CategoryRepository;
use App\Repositories\CommentRepository;
use App\Repositories\ProductRepository;
use App\Repositories\RatingRepository;
use App\Repositories\ThumbnailRepository;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    protected $productRepository;

    protected $categoryRepository;

    protected  $thumbnailRepository;

    protected $ratingRepository;

    protected $commentRepository;

    public function __construct(ProductRepository $productRepository,
                                CategoryRepository $categoryRepository,
                                ThumbnailRepository $thumbnailRepository,
                                RatingRepository $ratingRepository,
                                CommentRepository $commentRepository)
    {
        $this->productRepository    = $productRepository;

        $this->categoryRepository   = $categoryRepository;

        $this->thumbnailRepository  = $thumbnailRepository;

        $this->ratingRepository     = $ratingRepository;

        $this->commentRepository    = $commentRepository;
    }

    public function detailProduct($productId)
    {
        $product        = $this->productRepository->findById($productId);
        $categories     = $this->categoryRepository->getAll();
        $thumbnails     = $this->thumbnailRepository->getThumbnailProductId($productId);
        $category       = $this->productRepository->findCategoryByProductId($productId);
        $rating         = $this->ratingRepository->getRating($productId);
        $comments       = $this->commentRepository->getComment($productId);
        $now = Carbon::now();
        Carbon::setLocale('vi');

        return view('frontend.products.detail',[
            'product'       => $product,
            'categories'    => $categories,
            'thumbnails'    => $thumbnails,
            'category'      => $category,
            'rating'        => $rating,
            'comments'      => $comments,
            'now'           => $now,
        ]);
    }


    public function ajaxGetProductByCategory(Request $request)
    {
//        $products = $this->productRepository->getProductByCategory($request->get('cate_id'));
//        $now    = Carbon::now('Asia/Ho_Chi_Minh');
//        return view('frontend.ajax.products',[
//            'products' => $products
//        ]);
    }

}
