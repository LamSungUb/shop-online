<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Rating;
use App\Repositories\CommentRepository;
use App\Repositories\RatingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    protected $rating;

    protected $comment;

    public function __construct(RatingRepository $ratingRepository, CommentRepository $commentRepository)
    {
        $this->comment = $commentRepository;

        $this->rating = $ratingRepository;
    }

    public function review(Request $request,$productId)
    {

        $point = $request->input('rating');
        $comment = $request->input('comment');
        if ($point)
        {
            $this->rating->saveRating($productId,Auth::guard('customers')->user()->id,$point);
            $this->comment->saveComment($productId,Auth::guard('customers')->user()->id,$comment);
        }
        else
        {
            $this->comment->saveComment($productId,Auth::guard('customers')->user()->id,$comment);
        }

        return redirect()->back();
    }

}
