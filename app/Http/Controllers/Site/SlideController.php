<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Repositories\ImageSildeRepository;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    protected $imageSildeRepository;

    public function __construct(ImageSildeRepository $imageSildeRepository)
    {
        $this->imageSildeRepository = $imageSildeRepository;
    }

}
