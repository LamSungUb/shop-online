<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\Site\{RegisterRequest,LoginRequest,ChangePasswordRequest,ForgotPasswordRequest};


class CustomerController extends Controller
{
    protected $customerRepository;

    public function __construct(CustomerRepository $customerRepository){

        $this->customerRepository = $customerRepository;

    }
    /**
     * Xử lý đăng nhập
     */

    public function getLogin(){
        return view('frontend.account.login');
    }

    public function postLogin(LoginRequest $request){;
        $data = [
            'email'    => $request->input('email'),
            'password' => $request->input('password')
        ];
        // kiểm tra nếu tồn tại $data thì vào index ngược lại thì về trang login
        if (Auth::guard('customers')->attempt($data)) {
            return redirect()->route('index');
        } else {
            return redirect()->back()->with('warning', 'Email hoặc Mật Khẩu không chính xác');
        }
    }

    /**
     * Xử lý đăng ký
     */

    public function getRegister(){
        return view('frontend.account.register');
    }

    public function postRegister(RegisterRequest $request){
        // tạo mã confirmation_code
        $confirmation_code = time().uniqid(true);
        $customer = new Customer();
        $customer->name              = $request->name;
        $customer->email             = $request->email;
        $customer->password          = bcrypt($request['password']);
        $customer->confirmation_code = $confirmation_code;
        $email = $customer->email;
        $data = [
            'confirmation_code' => $confirmation_code,
        ] ;
        // gửi mail xác nhận (gửi kèm dữ liệu sang view, dữ liệu là kiểu mảng)
        Mail::send('form_email.email_register', $data, function($message) use ($email) {
            $message->to($email)->subject('XÁC NHẬN TÀI KHOẢN ĐĂNG KÝ');
        });
        $customer->save();
        // nếu thành công sẽ điều hướng về trang hiển thị link login vào gmail để xác nhận
        return redirect()->route('get.verify.email');
    }

    /**
     * xử lí, xác nhận tài khoản vừa đăng ký
     */
    public function postVerifyEmail($code){
        $customer = Customer::where('confirmation_code', $code)->first();
        if ($customer) {
            $customer->confirmed            = 1;
            $customer->confirmation_code    = null;
            $customer->save();
            Auth::guard('customers')->login($customer);
            return redirect()->route('index');
        }else{
            return "Code không tồn tại hoặc đã hết hạn";
        }
    }


    /**
     * Xử lý đăng xuất
     */
    public function Logout(){
        Auth::guard('customers')->logout();
        return redirect()->route('site.login');
    }


    /**
     * Xử lý phần quên mật khẩu
     */

    // b1 :form lấy lại mật khẩu người dùng
    public function getForgotPassword(){
        return view('frontend.account.email_forgot_password');
    }


    // b2: kiểm tra sự tồn tại của mật khẩu nếu tồn tại,
    // tạo ra mã code ngẫu nhiên, gửi mã code , email , url view upate mk
    // và đồng thời chuyển vào email xác thực sẽ điều hướng sang view update mật khẩu

    public function postForgotPassword(Request $request){
        $email = $request->email;
        $checkCustomer = Customer::where('email',$email)->first();
        if (!$checkCustomer){
            return redirect()->back()->with('warning','Email không tồn tại');
        }
        $code = bcrypt(md5(time().$email));
        $checkCustomer->code = $code;
        $checkCustomer->save();
        $url = route('site.form.forgot_password',[
            'code'  => $checkCustomer->code,
            'email' => $email
        ]);
        $data = [
            'route' => $url,
        ];
        Mail::send('form_email.email_verify_forgot_password',$data, function($message) use ($email) {
            $message->to($email)->subject('LẤY LẠI MẬT KHẨU');
        });
        return redirect()->route('mail.accept_forgot_password');
    }

    // b3: thực thi lấy mã code và email
    // truyền vào view update mật khẩu
    public function formForgotPassword( Request $request){
        $code = $request->get('code');
        $email = $request->get('email');
        return view('frontend.account.form_forgot_password',compact('code','email'));
    }

    // b4 : kiểm tra xem có đúng là email và code,
    // nếu đúng thì tiến hành thay đổi mật khẩu
    public function verifyForgotPassword(ForgotPasswordRequest $request){
        $code   = $request->code;
        $email  = $request->email;
        $verifyPassword = Customer::where('email',$email)->where('code',$code)->exists();
        if ($verifyPassword){
            $newPass = Customer::where('email',$email);
            $newPass->update([
                'password'  => bcrypt($request['password']),
            ]);
            return redirect()->route('site.login')->with('success','Lấy lại khẩu thành công');
        }else{
            return redirect()->back()->with('warning','Vui lòng kiểm tra lại Email');
        }
    }


    /**
     * Xử lý phần thay đổi mật khẩu
     */
    public function getChangePassword(){
        return view('frontend.account.change_password');
    }

    public function postChangePassword(ChangePasswordRequest $request,$id){
        if(Hash::check($request->password_old, Auth::guard('customers')->user()->password)){
            $newPass = Customer::find($id);
            $newPass->password = bcrypt($request->password);
            $newPass->save();
            Auth::guard('customers')->logout();
            return redirect()->route('site.login')->with('success','Đổi mật khẩu thành công');
        }
        return redirect()->back()->with('warning','Mật khẩu cũ không chính xác');
    }
}
