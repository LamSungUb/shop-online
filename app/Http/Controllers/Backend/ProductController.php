<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ThumbnailRepository;
use App\Services\ImageProductServices;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\Backend\{CreateProductRequest, UpdateProductRequest};
use phpDocumentor\Reflection\Types\This;

class ProductController extends Controller
{
    protected $productRepository;

    protected $categoryRepository;

    protected $thumbnailRepository;

    public function __construct(ProductRepository  $productRepository, CategoryRepository $categoryRepository, ThumbnailRepository $thumbnailRepository)
    {
        $this->productRepository = $productRepository;

        $this->categoryRepository = $categoryRepository;

        $this->thumbnailRepository = $thumbnailRepository;
    }

    public function list()
    {
        $categories = $this->categoryRepository->getAll();

        $products = $this->productRepository->getAllProducts();

        return view('backend.product.list',[
            'products'      => $products,
            'categories'    => $categories
        ]);

    }

    public function create()
    {
        $categories = $this->categoryRepository->getAll();

        return view('backend.product.create',[
            'categories'    => $categories
        ]);
    }

    public function store(CreateProductRequest $request)
    {
        $product = $request->all();

        if ($request->file('image')->isValid())
        {
            $product['image'] = ImageProductServices::addImage($request->file('image'));
        }
        $productId = $this->productRepository->addProduct($product);

        if ($request->hasFile('fileUpload'))
        {
            $thumbnails = $request->file('fileUpload');

            foreach ($thumbnails as $thumbnail)
            {
                $name = ImageProductServices::addThumbnail($thumbnail);

                $this->thumbnailRepository->createProductThumbnail($productId,$name);
            }
        }
        return redirect()->route('admin.product.list')->with('success','Thêm sản phẩm thành công');
    }

    public function edit($id)
    {
        $thumb = $this->thumbnailRepository->getThumbnailProductId($id);

        $cateNotId = $this->categoryRepository->notCategoryId($id);

        $categories = $this->categoryRepository->getAll();

        $product = $this->productRepository->findById($id);

        return view('backend.product.edit',[
            'product'    => $product,
            'categories' => $categories,
            'cateNotId'  => $cateNotId,
            'thumb'      => $thumb
        ]);
    }

    public function update(UpdateProductRequest $request,$id)
    {
        $product = $this->productRepository->findById($id);

        $updateProduct = $request->all();

        if ($request->hasFile('image'))
        {
            if ($product->image != $request->file('image'))
            {
                unlink(public_path('/Images/product_image/') . $product->image);
            }
            $image = ImageProductServices::addImage($request->file('image'));

            $this->productRepository->update($id,$updateProduct,$image);
        }
        else
        {
           $this->productRepository->update($id,$updateProduct,$product->image);
        }

        return redirect()->route('admin.product.list')->with('success','Sửa sản phẩm thành công');
    }

    public function destroy($id)
    {
        $this->thumbnailRepository->deleteProductThumbnail($id);

        $this->productRepository->destroy($id);

        return redirect()->route('admin.product.list')->with('success','Xóa sản phẩm thành công');
    }

}
