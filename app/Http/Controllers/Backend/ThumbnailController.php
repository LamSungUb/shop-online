<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CreateThumbnailRequest;
use App\Repositories\ThumbnailRepository;
use App\Services\ImageProductServices;
use Illuminate\Http\Request;

class ThumbnailController extends Controller
{
    protected $thumbnailRepository;

    public function __construct( ThumbnailRepository $thumbnailRepository)
    {
        $this->thumbnailRepository = $thumbnailRepository;
    }

    public function create($productId)
    {
        return view('backend.product.add_thumbnail',[
            'productId' => $productId
        ]);
    }
    public function store(CreateThumbnailRequest $request,$productId)
    {
        if ($request->hasFile('image'))
        {
            $name = ImageProductServices::addThumbnail($request->file('image'));
        }
        $this->thumbnailRepository->createProductThumbnail($productId,$name);

        return redirect()->back()->with('success','Thêm ảnh thành công');
    }

    public function edit($id)
    {
        $thumb = $this->thumbnailRepository->getThumbnailById($id);

        return view('backend.product.edit_thumbnail',[
            'thumb' => $thumb
        ]);
    }

    public function update(Request $request,$id)
    {
        $thumbnail = $this->thumbnailRepository->getThumbnailById($id);

        if ($request->hasFile('image'))
        {
            if ($thumbnail->thumbnail_name != $request->file('image'))
            {
                unlink(public_path('/Images/product_thumbnail/') . $thumbnail->thumbnail_name);
            }
            $image = ImageProductServices::addThumbnail($request->file('image'));

            $this->thumbnailRepository->updateProductThumbnail($id,$image);
        }
        return redirect()->back()->with('success','Sửa ảnh thành công');
    }

    public function delete($id)
    {
        $this->thumbnailRepository->deleteThumbnail($id);

        return redirect()->back()->with('success','Xóa sản phẩm thành công');
    }
}
