<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Http\Requests\Backend\LoginRequest;
use App\Http\Requests\Backend\RegisteRequest;




class UserController extends Controller
{
    // hiển thị trang đăng nhập
    public function getLogin(){
        return view('backend.account.login');
    }
    // thực thi đăng nhập
    public function postLogin(LoginRequest $request){
        $data = [
            'email'    => $request->input('email'),
            'password' => $request->input('password')
        ];
        // kiểm tra nếu tồn tại $data thì vào index ngược lại thì về trang login
        if (Auth::attempt($data)) {
            return redirect()->route('admin.index');
        } else {
            return redirect()->back()->with('warning', 'Email hoặc Mật Khẩu không chính xác');
        }

    }
    // hiển thị trang đăng ký
    public  function getRegister(){
        return view('backend.account.register');
    }
    // thực thi đăng ký
    public  function postRegister(RegisteRequest $request){
        $user = new User();
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->password     = bcrypt($request['password']);
        $user->save();

    }
    // đăng xuất
    public function Logout()
    {
        Auth::logout();
        return redirect()->route('admin.login');
    }

}
