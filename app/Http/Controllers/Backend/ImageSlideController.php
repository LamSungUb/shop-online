<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\ImageSildeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests\Backend\CreateThumbnailRequest;
class ImageSlideController extends Controller
{
    protected $iamgeSlideRepository;

    public function __construct(ImageSildeRepository $iamgeSlideRepository)
    {
        $this->iamgeSlideRepository = $iamgeSlideRepository;
    }

    public function list()
    {
        $slides = $this->iamgeSlideRepository->getAllSlide();

        return view('backend.image_slide.list',[
            'slides' => $slides
        ]);
    }

    public function create()
    {
        return view('backend.image_slide.create');
    }

    public function store(Request $request)
    {
        if ($request->hasFile('slide'))
        {
            $name = Str::random(10).$request->file('slide')->getClientOriginalName();

            $request->file('slide')->move(public_path('Images/slide'),$name);

            $this->iamgeSlideRepository->save($name);
        }

        return redirect()->route('admin.slide.list')->with('success','Thêm thành công');
    }

    public function destroy($id)
    {
        unlink(public_path('/Images/slide/') .$this->iamgeSlideRepository->findById($id)->image);

        $this->iamgeSlideRepository->destroy($id);

        return redirect()->route('admin.slide.list')->with('success','Xóa thành công');
    }

    public function active(Request $request)
    {
         DB::table('image_slides')
             ->where('id',$request->get('id'))
             ->update([
            'active' => $request->get('active')
        ]);
    }
}
