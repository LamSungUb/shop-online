<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryRepository;

    function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $category = $this->categoryRepository->getAll();
        return view('backend.category.category',compact('category'));
    }

    public function create(Request $request)
    {
        $create = $request->only([
            'category_name'
        ]);
        $this->categoryRepository->save($create);
        return redirect()->back()->with('success','Thêm danh mục thành công');
    }

    public function update(Request $request, $id)
    {
        $update = $request->only([
            'category_name'
        ]);
        $this->categoryRepository->save($update,$id);
        return redirect()->back()->with('success','Cập nhập danh mục thành công');
    }


    public function destroy($id)
    {
        $this->categoryRepository->destroy([$id]);
        return redirect()->back()->with('success','Xóa danh mục thành công');
    }
}
