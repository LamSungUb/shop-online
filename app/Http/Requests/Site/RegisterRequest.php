<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required|string|max:255',
            'email'          => 'required|string|email|max:255|unique:customers,email',
            'password'       => 'required|string|min:1|confirmed'
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => 'Họ và tên là trường bắt buộc',
            'name.max'              => 'Họ và tên không quá 255 ký tự',
            'email.required'        => 'Email là trường bắt buộc',
            'email.unique'          => 'Email đã tồn tại',
            'password.min'          => 'Mật khẩu phải chứa ít nhất 1 ký tự',
            'password.confirmed'    => 'Xác nhận mật khẩu không đúng',
            'password.required'     => 'Password là trường bắt buộc'
        ];
    }
}
