<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_old'   => 'required',
            'password'       => 'required|string|min:1|confirmed'
        ];
    }
    public function messages()
    {
        return [
            'password_old.required' => 'Vui lòng nhập mật khẩu cũ',
            'password.min'          => 'Mật khẩu phải chứa ít nhất 1 ký tự',
            'password.confirmed'    => 'Xác nhận mật khẩu không đúng',
            'password.required'     => 'Password là trường bắt buộc'
        ];
    }
}
