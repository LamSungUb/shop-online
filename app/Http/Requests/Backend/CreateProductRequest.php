<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'product_name'          => 'required|string',
            'image'                 => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'price'                 => 'required|numeric',
            'discount'              => 'numeric',
            'category_id'           => 'required',
            'quantity'              => 'required|numeric',
            'description'           => 'required',
            'fileUpload'            => 'required',
        ];
    }
    public function messages()
    {
        return [
            'product_name.required'         => 'Tên sản phẩm là trường bắt buộc',
            'product_name.string'           => 'Tên sản phẩm không đúng định dạng',
            'image.required'                => 'Ảnh là trường bắt buộc',
            'image.image'                   => 'Ảnh không đúng định dạng',
            'image.mimes'                   => 'Ảnh phải có đuôi .jpeg, .png, .jpg, .gif, .svg',
            'price.required'                => 'Giá không được để chống',
            'price.numeric'                 => 'Giá phải là kiểu số',
            'discount.numeric'              => 'Giá khuyến mãi phải là kiểu số',
            'category_id.required'          => 'Danh mục không được để chống',
            'quantity.required'             => 'Số lượng không được để chống',
            'description.required'          => 'Mô tả không được để chống',
            'quantity.numeric'              => 'Số lượng phải là kiểu số',
            'fileUpload.required'           => 'Ảnh nhỏ là trường bắt buộc',
            'fileUpload.image'              => 'Ảnh nhỏ không đúng định dạng',
            'fileUpload.mimes'              => 'Ảnh nhỏ phải có đuôi .jpeg, .png, .jpg, .gif, .svg',
        ];
    }
}
