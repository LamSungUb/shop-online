<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'          => 'required|string|email|max:255',
            'password'       => 'required|string|min:1'
        ];
    }
    public function messages()
    {
        return [
            'email.required'        => 'Email là trường bắt buộc',
            'email.email'           => 'Email không đúng định dạng',
            'password.min'          => 'Mật khẩu phải chứa ít nhất 1 ký tự',
            'password.required'     => 'Password là trường bắt buộc'
        ];
    }
}
