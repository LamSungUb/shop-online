<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateThumbnailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
        'image'                 => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ];
    }
    public function messages()
    {
        return [
            'image.required'                => 'Ảnh là trường bắt buộc',
            'image.image'                   => 'Ảnh không đúng định dạng',
            'image.mimes'                   => 'Ảnh phải có đuôi .jpeg, .png, .jpg, .gif, .svg',
        ];
    }
}
