<?php

use Illuminate\Support\Facades\Route;

/**
 * Web
 */

/**
 * Login web
 */
Route::group(['namespace' => 'Site'], function () {

    Route::get('login','CustomerController@getLogin')->name('site.login');
    Route::post('post-login','CustomerController@postLogin')->name('site.post.login');


    Route::get('register','CustomerController@getRegister')->name('site.register');
    Route::post('post-register','CustomerController@postRegister')->name('site.post.register');
    /**
     * view bấm vào gmail để chấp nhận đăng ký tìa khoản
     */
    Route::get('verify-account',function (){
        return view('frontend.mail.accept_account_register');
    })->name('get.verify.email');
    /**
     * Thực hiện nhiệm vụ xác nhận người dùng = mã code truyền sang
     */
    Route::get('verify-account/{code}', 'CustomerController@postVerifyEmail');
    /**
     * route view nhập email - đổi mật khẩu
     */
    Route::get('form-email-forgot-password','CustomerController@getForgotPassword')->name('site.forgot_password');
    /**
     * thực hiện kiểm tra email - đổi mật khẩu và gửi email kèm mã code vào gmail
     */
    Route::post('post-forgot-password','CustomerController@postForgotPassword')->name('site.post.forgot_password');
    /**
     * view bấm vào gmail để đổi mk
     */
    Route::get('accept-forgot-password',function (){
        return view('frontend.mail.accept_forgot_password');
    })->name('mail.accept_forgot_password');
    /**
     * hiển thị view đổi mk
     */
    Route::get('form-forgot-password','CustomerController@formForgotPassword')->name('site.form.forgot_password');
    /**
     * thực hiện đổi mk nếu đúng email và code
     */
    Route::post('verify-forgot-password','CustomerController@verifyForgotPassword')->name('site.verify.forgot.password');

    Route::get('logout','CustomerController@Logout')->name('site.logout');

});
/**
 * Xử lý bên trang Web
 */
Route::group(['namespace' => 'Site', 'prefix' => 'account'], function () {

    Route::get('change-password','CustomerController@getChangePassword')->name('site.change_password');
    Route::post('post-change-password-{id}','CustomerController@postChangePassword')->name('site.post.change_password');

});

Route::group(['namespace' => 'Site'],function (){
    Route::get('index','HomeController@index')->name('index');
    Route::get('/','HomeController@index')->name('index');
    Route::get('ajax-products','ProductController@ajaxGetProductByCategory')->name('site.ajax.product');
});

Route::group(['namespace' => 'Site','prefix' => 'product'],function (){
    Route::get('{ProductId}/detail','ProductController@detailProduct')->name('site.product.detail');
    Route::post('{productID}/comment','ReviewController@review')->name('site.comment');
});

