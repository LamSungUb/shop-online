<?php

use Illuminate\Support\Facades\Route;

/**
 * Trang Admin
 */

/**
 * Login admin
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin'], function ()
{
    Route::get('login','UserController@getLogin')->name('admin.login');
    Route::post('post-login','UserController@postLogin')->name('admin.post.login');
    Route::get('register','UserController@getRegister')->name('admin.register');
    Route::post('post-register','UserController@postRegister')->name('admin.post.register');
    Route::get('logout','UserController@Logout')->name('admin.logout');
});
/**
 * Xử lý bên trang Admin
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin'], function ()
{
    Route::get('index','HomeController@index')->name('admin.index');
    Route::group(['prefix' => 'category'],function ()
    {
        Route::get('list','CategoryController@index')->name('admin.category');
        Route::post('create','CategoryController@create')->name('admin.category.create');
        Route::post('{id}/update','CategoryController@update')->name('admin.category.update');
        Route::get('{id}/destroy','CategoryController@destroy')->name('admin.category.destroy');
    });

    Route::group(['prefix' => 'image-slide'],function ()
    {
        Route::get('list',function ()
        {
            return view('backend.image_slide.image_slide');
        });
    });

    Route::group(['prefix' => 'product'], function ()
    {
        Route::get('list','ProductController@list')->name('admin.product.list');
        Route::get('create','ProductController@create')->name('admin.product.create');
        Route::post('store','ProductController@store')->name('admin.product.store');
        Route::get('{id}/edit','ProductController@edit')->name('admin.product.edit');
        Route::post('{id}/update','ProductController@update')->name('admin.product.update');
        Route::get('{id}/destroy','ProductController@destroy')->name('admin.product.destroy');

        Route::get('{productID}/create-thumbnail','ThumbnailController@create')->name('admin.thumb.create');
        Route::post('{productID}/store-thumbnail','ThumbnailController@store')->name('admin.thumb.store');
        Route::get('{thumbID}/edit-thumbnail','ThumbnailController@edit')->name('admin.thumb.edit');
        Route::post('{thumbID}/update-thumbnail','ThumbnailController@update')->name('admin.thumb.update');
        Route::get('{thumbID}/delete-thumbnail','ThumbnailController@delete')->name('admin.thumb.delete');

    });

    Route::group(['prefix' => 'slide'], function ()
    {
        Route::get('list','ImageSlideController@list')->name('admin.slide.list');
        Route::get('create','ImageSlideController@create')->name('admin.slide.create');
        Route::post('store','ImageSlideController@store')->name('admin.slide.store');
        Route::get('{id}/edit','ImageSlideController@edit')->name('admin.slide.edit');
        Route::post('{id}/update','ImageSlideController@update')->name('admin.slide.update');
        Route::get('{id}/destroy','ImageSlideController@destroy')->name('admin.slide.destroy');
        Route::get('active','ImageSlideController@active')->name('admin.slide.active');
    });

});

